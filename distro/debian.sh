#!/bin/bash
comment="The-Universal-Operating-System"
debian_repo="https://deb.debian.org/debian"
debian_dist="stable"
tool_init(){
    which debootstrap && return 0
    wget https://salsa.debian.org/installer-team/debootstrap/-/archive/master/debootstrap-master.tar.gz
    tar -xf debootstrap-master.tar.gz
    make -C debootstrap-master install
}
get_profiles(){
    echo -ne " gnome full-gnome false"
    echo -ne " gnome-core minimal-gnome false"
    echo -ne " xfce full-xfce false"
    echo -ne " kde kde-desktop false"
    echo -ne " cinnamon cinnamon-desktop false"
    echo -ne " lightdm light-display-manager false"
    echo -ne " liquorix liquorix-kernel false"
    echo -ne " network-manager Network-Manager true"
    echo -ne " sudo sudo-command false"
    echo -ne " nonfree-firmware Nonfree-drivers false"
    echo -ne " no-recommends Dont-install-recommends true"
}
installation(){
    if [[ ! -f /usr/share/debootstrap/scripts/${debian_dist} ]] ; then
        ln -s sid /usr/share/debootstrap/scripts/${debian_dist}
    fi
    if [[ ! -f /target/etc/os-release ]] ; then
        debootstrap --arch=amd64 --no-check-gpg --extractor=ar "${debian_dist}" /target "${debian_repo}"
    fi
    chroot /target apt install gnupg -y
    if echo ${profiles[@]} | grep liquorix &>/dev/null ; then
        curl https://liquorix.net/liquorix-keyring.gpg | chroot /target apt-key add -
        echo "deb http://liquorix.net/debian testing main" > chroot/etc/apt/sources.list.d/liquorix.list
    else
        chroot /target apt install linux-image-amd64 -y
    fi
    if echo ${profiles[@]} | grep no-recommends &>/dev/null ; then
        echo 'APT::Install-Recommends "0";' > /target/etc/apt/apt.conf.d/01norecommend
        echo 'APT::Install-Suggests "0";' >> /target/etc/apt/apt.conf.d/01norecommend
    fi
    chroot /target apt update
    chroot /target apt install grub-pc-bin grub-efi grub-efi-ia32-bin -yq
}
# distro config
distro_configure(){
    TITLE="debian" menu "0 exit"
}

populate_user(){
    for grp in audio video netdev sudo plugdev bluetooth ; do
        usermod -aG $grp "$user" || true
    done
}

# profiles
install_nonfree-firmware(){
    echo "deb ${debian_repo} ${debian_dist} main contrib non-free non-free-firmware" > /target/etc/apt/sources.list.d/nonfree.list
    chroot /target apt update
    chroot /target apt install -y firmware-amd-graphics firmware-atheros \
        firmware-bnx2 firmware-bnx2x firmware-brcm80211  \
        firmware-cavium firmware-intel-sound firmware-intelwimax \
        firmware-ipw2x00 firmware-ivtv firmware-iwlwifi \
        firmware-libertas firmware-linux firmware-linux-free \
        firmware-linux-nonfree firmware-misc-nonfree firmware-myricom \
        firmware-netxen firmware-qlogic firmware-realtek firmware-samsung \
        firmware-siano firmware-ti-connectivity firmware-zd1211
}
install_sudo(){
    chroot /target apt-get install sudo -y
}
install_network-manager(){
    chroot /target apt-get install network-manager -y
}
install_kde(){
    chroot /target apt-get install kde-standard network-manager -y
}

install_cinnamon(){
    chroot /target apt install cinnamon -y
}

install_lightdm(){
    chroot /target apt-get install lightdm lightdm-gtk-greeter -y
}

install_xfce(){
    chroot /target apt-get install xfce4 xfce4-goodies network-manager-gnome pulseaudio -y
}
install_gnome-core(){
    chroot /target apt-get install gnome-core network-manager -y
}

install_gnome(){
    chroot /target apt-get install gnome -y
}

