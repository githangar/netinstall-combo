#!/bin/bash
comment="I-use-Arch-btw."
arch_repo='https://mirrors.edge.kernel.org/archlinux/\$repo/os/\$arch'
tool_init(){
   if ! which archstrap &>/dev/null ; then
       wget -c "https://gitlab.com/tearch-linux/applications-and-tools/archstrap/-/raw/master/archstrap.sh" -O archstrap.sh
       install archstrap.sh /usr/bin/archstrap
   fi
}
get_profiles(){
    echo -ne " gnome gnome-desktop false"
    echo -ne " xfce xfce-desktop false"
    echo -ne " kde kde-desktop false"
    echo -ne " network-manager Network-Manager false"
    echo -ne " pulseaudio pulseaudio false"
    echo -ne " lightdm light-display-manager false"
    echo -ne " sddm Simple-Desktop-Display-Manager false"
}
installation(){
    if [[ ! -f /target/etc/os-release ]] ; then
        archstrap /target -r "${arch_repo}"
    fi
    chroot /target pacman --noconfirm -Sy linux grub efibootmgr
    mount --bind /dev /target/dev
    mount --bind /sys /target/sys
    mount --bind /proc /target/proc
    for dir in $(ls /target/lib/modules/) ; do
        depmod -a $dir
        ln -s ../lib/modules/$dir/vmlinuz /target/boot/vmlinuz-$dir
        chroot /target mkinitcpio -k $dir -g /boot/initrd.img-$dir
    done
    umount /target/dev /target/sys /target/proc
}

# distro config
distro_configure(){
    TITLE="archlinux" menu "0 exit"
}

populate_user(){
    for grp in audio video netdev wheel ; do
        usermod -aG $grp "$user" || true
    done
}

install_network-manager(){
    chroot /target pacman --noconfirm -S networkmanager
    chroot /target systemctl enable NetworkManager
}

install_gnome(){
    chroot /target pacman --noconfirm -S  xorg-xinit xorg xorg-server gnome
    chroot /target systemctl enable gdm
}
install_pulseaudio(){
    chroot /target pacman --noconfirm -S pulseaudio
}

install_lightdm(){
    chroot /target pacman --noconfirm -S lightdm lightdm-gtk-greeter
    chroot /target systemctl enable lightdm
}

install_sddm(){
    chroot /target pacman --noconfirm -S sddm
    chroot /target systemctl enable sddm
}

install_xfce(){
    chroot /target pacman --noconfirm -S  xorg-xinit  xorg xorg-server xfce4 xfce4-goodies
}

install_kde(){
    chroot /target pacman --noconfirm -S xorg-xinit xorg plasma plasma-wayland-session kde-applications
}

