#!/bin/bash
set -e
#### Check root
if [[ ! $UID -eq 0 ]] ; then
    echo -e "\033[31;1mYou must be root!\033[:0m"
    exit 1
fi
#### Remove all environmental variable
for e in $(env | sed "s/=.*//g") ; do
    unset "$e" &>/dev/null
done

#### Set environmental variables
export PATH=/bin:/usr/bin:/sbin:/usr/sbin
export LANG=C
export SHELL=/bin/bash
export TERM=linux
export DEBIAN_FRONTEND=noninteractive

#### Install dependencies
if which apt &>/dev/null && [[ -d /var/lib/dpkg && -d /etc/apt ]] ; then
    apt-get update
    apt-get install curl mtools squashfs-tools grub-pc-bin grub-efi xorriso debootstrap ssh -y
fi

set -ex
#### Chroot create
mkdir chroot || true

#### For devuan
debootstrap --variant=minbase --no-check-gpg --no-merged-usr --exclude=usrmerge --arch=amd64 testing chroot https://pkgmaster.devuan.org/merged
chroot chroot apt install devuan-keyring -y
echo "deb https://pkgmaster.devuan.org/merged testing main contrib non-free non-free-firmware" > chroot/etc/apt/sources.list
chroot chroot apt update
#### Set root password
pass="live"
chroot chroot useradd -m -s /bin/bash user
echo -e "$pass\n$pass\n" | chroot chroot passwd
echo -e "$pass\n$pass\n" | chroot chroot passwd user

#### Fix apt & bind
# apt sandbox user root
echo "APT::Sandbox::User root;" > chroot/etc/apt/apt.conf.d/99sandboxroot
for i in dev dev/pts proc sys; do mount -o bind /$i chroot/$i; done
chroot chroot apt-get install gnupg -y

#### For debian/devuan
chroot chroot apt-get install live-boot -y
echo "DISABLE_DM_VERITY=true" >> chroot/etc/live/boot.conf

#### Remove bloat files after dpkg invoke (optional)
cat > chroot/etc/apt/apt.conf.d/02antibloat << EOF
DPkg::Post-Invoke {"rm -rf /usr/share/locale || true";};
DPkg::Post-Invoke {"rm -rf /usr/share/man || true";};
DPkg::Post-Invoke {"rm -rf /usr/share/help || true";};
DPkg::Post-Invoke {"rm -rf /usr/share/doc || true";};
DPkg::Post-Invoke {"rm -rf /usr/share/info || true";};
DPkg::Post-Invoke {"rm -rf /usr/share/i18n || true";};
EOF

#### Configure system
cat > chroot/etc/apt/apt.conf.d/01norecommend << EOF
APT::Install-Recommends "0";
APT::Install-Suggests "0";
EOF

#### stock kernel
chroot chroot apt-get install linux-image-amd64 -y

#### Usefull stuff
chroot chroot apt-get install network-manager -y

#### Install netinstall-combo
chroot chroot apt-get install make wget zstd dialog nano fdisk binutils xz-utils curl -y
make install DESTDIR=chroot
rm -rf chroot/netinstall-combo/distro/*

#### Install drivers (free and non-free)
chroot chroot apt-get install -y zd1211-firmware firmware-linux firmware-netxen firmware-ralink \
firmware-realtek firmware-iwlwifi firmware-atheros firmware-bnx2 firmware-libertas \
bluez-firmware firmware-brcm80211 firmware-amd-graphics firmware-cavium firmware-intel-sound firmware-misc-nonfree \
firmware-myricom firmware-qlogic firmware-samsung firmware-siano firmware-sof-signed firmware-ti-connectivity


#### Clear logs and history
chroot chroot apt-get clean
rm -f chroot/root/.bash_history
rm -rf chroot/var/lib/apt/lists/*
find chroot/var/log/ -type f | xargs rm -f

### Remove gpu drivers
rm -rf  chroot/lib/modules/*/kernel/drivers/gpu
chroot chroot depmod -a $(ls chroot/lib/modules)
chroot chroot update-initramfs -u -k all

#### Create squashfs
mkdir -p isowork/boot || true
for dir in dev dev/pts proc sys ; do
    while umount -lf -R chroot/$dir 2>/dev/null ; do true; done
done
mksquashfs chroot filesystem.squashfs -comp gzip -wildcards
mkdir -p isowork/live || true
mv filesystem.squashfs isowork/live/filesystem.squashfs

#### Copy kernel and initramfs (Debian/Devuan)
cp -pf chroot/boot/initrd.img-* isowork/boot/initrd.img
cp -pf chroot/boot/vmlinuz-* isowork/boot/vmlinuz

#### Write grub.cfg
mkdir -p isowork/boot/grub/
echo 'insmod all_video' > isowork/boot/grub/grub.cfg
echo 'set timeout=3' >> isowork/boot/grub/grub.cfg
echo 'menuentry "Start Netinstall-combo" --class linux {' >> isowork/boot/grub/grub.cfg
echo '    linux /boot/vmlinuz boot=live quiet init=/netinstall-combo/init.sh' >> isowork/boot/grub/grub.cfg
echo '    initrd /boot/initrd.img' >> isowork/boot/grub/grub.cfg
echo '}' >> isowork/boot/grub/grub.cfg

#### Create iso
grub-mkrescue isowork -o netinstall-combo-$(date +%s).iso
