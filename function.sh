export LANG=C
export NO_COLOR=1
set -e
height="16"
width="0"
menu(){
    dialog  --no-cancel --ok-label "Select" --output-fd 1 --menu "$TITLE" $height $width 0 $@
}
list(){
    dialog --no-cancel --ok-label "Select" --output-fd 1 --checklist "$TITLE" 0 $width 0 $@
}
input(){
    dialog --no-cancel --ok-label "Select" --output-fd 1 --inputbox "$TITLE" 0 $width "" $@
}
run(){
    dialog --ok-label "Continue" --programbox "$TITLE" 1000 1000 || true
}
select_disk(){
    [[ "$TITLE" == "" ]] && TITLE="Select disk"
    for i in $(ls /sys/block | grep -v loop | grep -v fd | grep -v sr) ; do
        echo -ne "$i $(cat /sys/block/$i/size) "
    done | xargs dialog --ok-label "Select" --no-cancel --output-fd 1 --menu "$TITLE" $height $width 0
}
select_part(){
    [[ "$TITLE" == "" ]] && TITLE="Select partition"
    for i in $(ls /sys/block/$1) ; do
        [[ -f /sys/block/$1/$i/size ]] && echo -ne "$i $(cat /sys/block/$1/$i/size) "
    done | xargs dialog --ok-label "Select" --no-cancel --output-fd 1 --menu "$TITLE" $height $width 0
}

msg(){
    dialog --msgbox "$@" $height $width
}
msg_console(){
    echo -ne "\033[31;1m$@\033[;0m"
    read -n 1
}
