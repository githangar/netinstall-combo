#!/bin/bash
export PROGRAM=$(pwd)
export DEBIAN_FRONTEND=noninteractive
source "${PROGRAM}"/function.sh
source "${PROGRAM}"/instmenu.sh
source "${PROGRAM}"/diskmenu.sh
cd /tmp
dialog --ok-label "Start" --msgbox "Welcome to Multi NetInstaller\nPress enter to start installation" $(($height/2)) $width
selfupdate(){
    wget https://gitlab.com/tearch-linux/applications-and-tools/netinstall-combo/-/archive/master/netinstall-combo-master.tar.gz -O /tmp/combo.tar.gz || return
    cd /tmp
    tar -xf combo.tar.gz
    cd netinstall-combo-master
    make install
    source "${PROGRAM}"/function.sh
    source "${PROGRAM}"/instmenu.sh
    source "${PROGRAM}"/diskmenu.sh
}
selfupdate || true
main(){
    opt=$(TITLE="Multi NetInstall Main Menu" menu 1 "Open-Terminal" 2 "Installation" \
                 3 "Network-Manager" 4 "Disk-Menu" 5 "Self-Update" 0 "Exit")
    case $opt in
        1) # Open Terminal
            echo -en "\033c" && clear
            bash --version
            echo -e "\033[;34mYou are in bash terminal. If you want to quit, run exit command. \033[34;1m"
            PS1="\[\033[;31m\]>>> \[\033[;32m\]" /bin/bash --norc --noprofile
            ;;
        2) # Installation
            instmenu
            ;;
        3) # Network Manager
            nmtui
            ;;
        4) # Partitioning
             diskmenu
             ;;
        5) # self-update
             selfupdate
             ;;
        0) # exit
            sync
            reboot -f
            ;;
    esac
    main
}
main
